import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RepairsRoutingModule } from './repairs-routing.module';
import { SharedModule } from '../shared/shared.module';
import { RepairsComponent } from './pages/repairs/repairs.component';


@NgModule({
  declarations: [RepairsComponent],
  imports: [
    CommonModule,
    RepairsRoutingModule,
    SharedModule
  ]
})
export class RepairsModule { }
