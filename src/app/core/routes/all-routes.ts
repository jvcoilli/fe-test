import { Routes } from "@angular/router";

export const ALL_ROUTES: Routes = [
    { path: 'repairs', loadChildren: () => import('../../repairs/repairs.module').then(m => m.RepairsModule) }, 
    { path: 'orders', loadChildren: () => import('../../orders/orders.module').then(m => m.OrdersModule) },
    { path: '', redirectTo: 'orders', pathMatch: 'full'}
];
