import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';
import { MaterialModule } from './modules/material/material.module';
import { NulldashPipe } from './pipes/nulldash.pipe';
import { CustomCurrencyPipe } from './pipes/custom-currency.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [NulldashPipe, CustomCurrencyPipe],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    CurrencyPipe,
    CustomCurrencyPipe
],
  exports: [NulldashPipe, CustomCurrencyPipe, MaterialModule, FormsModule, ReactiveFormsModule]
})
export class SharedModule { }
