import { CurrencyPipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customCurrency'
})
export class CustomCurrencyPipe implements PipeTransform {
  constructor(private cp: CurrencyPipe) {}
    transform(value: any, round?: boolean): any {
        if (value === 0 || value === '0') {
            return round ? '€ 0' : '€ 0,00';
        }
        if (!value) {
            return '-';
        } else {
            let returnValue = this.cp.transform(parseFloat(value).toString(), '€ ').replace(/,/g, '.');
            const n = returnValue.lastIndexOf('.');
            if (n >= 0 && returnValue.length) {
                returnValue = returnValue.substring(0, n) + ',' + returnValue.substring(n + 1);
            }
            if (round) {
                returnValue = returnValue.substring(0, returnValue.length - 3);
            }
            return returnValue;
        }
    }
}
