import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nulldash'
})
export class NulldashPipe implements PipeTransform {

  transform(value: any): any {
    if (!value) {
      return '-';
    }

    return value;
  }

}
