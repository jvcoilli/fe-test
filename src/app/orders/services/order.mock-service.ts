import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Order } from '../models/order.model';
import { AbstractOrderService } from './order.abstract-service';

@Injectable()
export class MockOrderService implements AbstractOrderService {
  constructor() { }

  public getOrders(): Observable<Order[]> {
    const orders = Array.from({length: 100}, (_, k) => createNewOrder(k + 1));
    return of([...orders]);
  }

  public getStatuses(): Observable<string[]> {
    return of([...STATUSES]);
  }
}

const STATUSES: string[] = [
  'New',
  'Ordered',
  'Cancelled',
];

const VEHICLE_TYPES: string[] = [
  'Car',
  'Bike',
  'Moto',
];

const LEASING_COMPANIES: string[] = [
  'Arval',
  'Leaseplan',
  'KBC Autolease',
  'Athlon'
];

const SUPPLIERS: string[] = [
  'Volkswagen',
  'Tesla',
  'Mercedes'
];

const VEHICLE_MODELS: string[] = [
  'CLA SB',
  'Y',
  'Tiguan'
];

function createNewOrder(id: number): Order {
  return {
    status: STATUSES[Math.round(Math.random() * (STATUSES.length - 1))],
    leasingCompany: LEASING_COMPANIES[Math.round(Math.random() * (LEASING_COMPANIES.length - 1))],
    supplier: SUPPLIERS[Math.round(Math.random() * (SUPPLIERS.length - 1))],
    creationDate: randomDate(new Date(2022, 0, 1), new Date()),
    vehicleType: VEHICLE_TYPES[Math.round(Math.random() * (VEHICLE_TYPES.length - 1))],
    isReady: Math.random() > 0.5 ? true : false,
    vehicleModel: VEHICLE_MODELS[Math.round(Math.random() * (VEHICLE_MODELS.length - 1))],
    carValueEuros: Math.round(Math.random() * 25000)
  };

}

function randomDate(start, end) {
  return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}
