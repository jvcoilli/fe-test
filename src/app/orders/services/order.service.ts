import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Order } from '../models/order.model';
import { AbstractOrderService } from './order.abstract-service';

@Injectable()
export class OrderService implements AbstractOrderService {
  
  constructor(private http: HttpClient) { }

  public getOrders(): Observable<Order[]> {
    return this.http.get<Order[]>('api/orders');
  }

  public getStatuses(): Observable<string[]> {
    return this.http.get<string[]>('api/orders/statuses');
  }
}
