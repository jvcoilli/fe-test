import { Observable } from "rxjs";
import { Order } from "../models/order.model";

export abstract class AbstractOrderService {
    public abstract getOrders(): Observable<Order[]>;
    public abstract getStatuses(): Observable<string[]>;
  }