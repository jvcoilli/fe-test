import { Component, EventEmitter, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AbstractOrderService } from '../../services/order.abstract-service';

@Component({
  selector: 'app-order-dialog',
  templateUrl: './order-dialog.component.html',
  styleUrls: ['./order-dialog.component.scss']
})
export class OrderDialogComponent implements OnInit {

  myForm: FormGroup;

  statuses: string[];

  public event: EventEmitter<any> = new EventEmitter();

  constructor(
    public dialogRef: MatDialogRef<OrderDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private orderService: AbstractOrderService,
    private fb: FormBuilder,  
  ) { }

  ngOnInit(): void {
    this.myForm = this.fb.group({  
      name: ['', [Validators.required, Validators.minLength(2)]],  
      nameOptions: ['yes'],  
      status: ['', Validators.required],  
    }); 

    this.getStatussen();
  }

  getStatussen():void {
    this.orderService.getStatuses().subscribe((next) => {
      this.statuses = next;
    });;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    this.event.emit({data: this.myForm.value});
    this.dialogRef.close();
  }

  get form() {  
    return this.myForm.controls;  
  } 

  changeNameOption(e) {
    if(e.value == 'no') {
      this.form.name.disable();
    } else {
      this.form.name.enable();
    }

  }

}
