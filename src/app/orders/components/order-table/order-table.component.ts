import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { Observable } from 'rxjs';
import { Order } from '../../models/order.model';
import { AbstractOrderService } from '../../services/order.abstract-service';

@Component({
  selector: 'app-order-table',
  templateUrl: './order-table.component.html',
  styleUrls: ['./order-table.component.scss']
})
export class OrderTableComponent implements AfterViewInit {

  orders: Order[];
  ready: boolean = false;

  displayedColumns: string[] = ['status', 'leasingCompany', 'supplier', 'vehicleType', 'isReady', 'vehicleModel', 'carValueEuros', 'creationDate'];
  dataSource: MatTableDataSource<Order>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private orderService: AbstractOrderService) {
    this.getOrders();
    this.dataSource = new MatTableDataSource(this.orders);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'creationDate': {
          let newDate = new Date(item.creationDate);
          return newDate;
        }
        default: {
          return item[property];
        }
      }
    };
  }

  private getOrders(): void {
    this.orderService.getOrders().subscribe((next) => {
      this.orders = next;
      this.ready = true;
    });;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
