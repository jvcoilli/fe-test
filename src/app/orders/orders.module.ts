import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrdersRoutingModule } from './orders-routing.module';
import { SharedModule } from '../shared/shared.module';
import { OrderDialogComponent } from './components/order-dialog/order-dialog.component';
import { OrdersComponent } from './pages/orders/orders.component';
import { OrderTableComponent } from './components/order-table/order-table.component';
import { environment } from 'src/environments/environment';
import { AbstractOrderService } from './services/order.abstract-service';


@NgModule({
  declarations: [OrdersComponent, OrderTableComponent, OrderDialogComponent],
  imports: [
    CommonModule,
    OrdersRoutingModule,
    SharedModule
  ],
  providers: [
    { provide: AbstractOrderService, useClass: environment.orderService }
  ]
})
export class OrdersModule { }
