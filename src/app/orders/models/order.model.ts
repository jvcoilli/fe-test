export type Order = {
    status?: string;
    leasingCompany?: string;
    supplier?: string;
    creationDate?: Date;
    vehicleType?: string;
    isReady?: boolean;
    vehicleModel?: string;
    carValueEuros?: number;
};