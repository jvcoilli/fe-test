import { OrderService } from "src/app/orders/services/order.service";

export const environment = {
  apiUrl: 'api-url.test',
  production: false,
  orderService: OrderService
};