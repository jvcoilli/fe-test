import { OrderService } from "src/app/orders/services/order.service";

export const environment = {
  apiUrl: 'api-url-prod.test',
  production: true,
  orderService: OrderService
};