import { MockOrderService } from "src/app/orders/services/order.mock-service";

export const environment = {
  apiUrl: 'api-url-tst.test',
  production: false,
  orderService: MockOrderService
};