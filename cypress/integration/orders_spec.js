describe('Order Test', () => {
  beforeEach(() => {
    cy.visit('http://localhost:4200')
  })

    it('The TechnicalTest app default goes to orders page', () => {
      cy.url().should('include', '/orders') // => true
    })

    it('Add order name field is hidden if no is selected en visible if yes is selected', () => {
      cy.contains('Add Order').click()
      cy.get('input[name="name"]').should('be.visible')
      cy.get('mat-radio-button[name="nameOptionsNo"]').click()
      cy.get('input[name="name"]').should('not.exist')
      cy.get('mat-radio-button[name="nameOptionsYes"]').click()
      cy.get('input[name="name"]').should('be.visible')
    })
  })